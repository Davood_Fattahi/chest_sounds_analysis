function pxx_smooth=get_maf(pxx)
%% Moving Average Filter (MAF)
pxx_smooth = smooth(pxx);
end