function S=separability_target(X,M,P)
%% Paper Information
% Unsupervised SIngle-Channel Separation of Non-Stationary Signals using Gammatone Filterbank and Itakura Saito Non-negative Matrix Two-Dimensional Factorization
% https://www.researchgate.net/profile/Wai-Lok-Woo/publication/236007062_Unsupervised_Single-Channel_Separation_of_Nonstationary_Signals_Using_Gammatone_Filterbank_and_Itakura-Saito_Nonnegative_Matrix_Two-Dimensional_Factorizations/links/5efe1aed45851550508533a6/Unsupervised-Single-Channel-Separation-of-Nonstationary-Signals-Using-Gammatone-Filterbank-and-Itakura-Saito-Nonnegative-Matrix-Two-Dimensional-Factorizations.pdf

%% Purpose
% For NMF methods, to measure the separability of target source x in
% presence of interferring sources p


%% Inputs
% X= time frequency representaiton of target
% M= time frequency mask generated in NMF method which is used in the
% mixture recording y to extract the desired target (X_opt= M.*Y)
% P= time frequency representation of interferring sources

% Output
% S= separability. Value of greater than 0.8 considered satisfactory

S=norm(M.*X,'fro')^2/norm(X,'fro')^2-norm(M.*P,'fro')^2/norm(X,'fro')^2;
end